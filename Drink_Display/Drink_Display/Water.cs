﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    class Water : Drink
    {
        public override string Description
        {
            get
            {
                return _name + ", " + Carbonated + ", " + "flavored with " + _flavoredWith;
            }
        }
        private string _flavoredWith;

        public Water(string name, bool carbonated, string flavor) : base(name, carbonated) 
        {
            _flavoredWith = flavor;
        }
    }
}
