﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    class Milk : Drink
    {
        public override string Description
        {
            get
            {
                return _name + ", " + Carbonated + ", " + _flavor + ", " + String.Format("{0}%", _fat);
            }
        }

        private decimal _fat;
        private string _flavor;

        public Milk(string name, bool carbonated, decimal fatPercent, string flavor = "Plain") : base(name, carbonated)
        {
            _fat = fatPercent;
            _flavor = flavor;
        }
    }
}
