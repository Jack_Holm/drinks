﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    class Soda : Drink
    {
        public override string Description
        {
            get
            {
                return _name + ", " + Carbonated;
            }
        }

        public Soda(string name, bool carbonated) : base(name,carbonated){ }
    }
}
