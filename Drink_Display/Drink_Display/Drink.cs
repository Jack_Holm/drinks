﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    abstract class Drink
    {
        public abstract string Description { get; }
        public string Name { get { return _name; } }
        public string Carbonated
        {
            get
            {
                return _carbonated ? "carbonated" : "not carbonated";
            }
        }
        protected string _name;
        protected bool _carbonated;
        
        protected Drink(string name, bool carbonated)
        {
            _name = name;
            _carbonated = carbonated;
        }
    }
}
