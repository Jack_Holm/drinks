﻿using System;
using System.Collections.Generic;

namespace Drink_Display
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Drink> drinks = new List<Drink>();
            drinks.Add(new Juice("Orange Juice", false, "oranges"));
            drinks.Add(new Beer("Budweiser", true, 5));
            drinks.Add(new Soda("Pepsi", true));
            drinks.Add(new Water("Water", true, "lemon"));
            drinks.Add(new Milk("Whole Milk", false, 3.25M));
            foreach (Drink d in drinks)
                Console.WriteLine(d.Description);

        }
    }
}
