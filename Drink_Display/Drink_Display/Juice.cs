﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    class Juice : Drink
    {
        public override string Description
        {
            get
            {
                return _name + ", " + Carbonated + ", made from " + _fruitName;
            }
        }
        public string FruitName { get { return _fruitName; } }
        private string _fruitName;

        public Juice(string name, bool carbonated, string fruitName) : base(name, carbonated)
        {
            _fruitName = fruitName;
        }
    }
}
