﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Drink_Display
{
    class Beer : Drink
    {
        public override string Description
        {
            get
            {
                return _name + ", " + Carbonated + ", " + String.Format("{0}%", _alcohol);
            }
        }
        public decimal AlcoholPercentage { get { return _alcohol; } }
        private decimal _alcohol;

        public Beer(string name, bool carbonated, decimal alcoholPercent) : base(name, carbonated)
        {
            _alcohol = alcoholPercent;
        }
    }
}
